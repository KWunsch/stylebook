# Das offizielle Stylebook des Zentrums für Digitale Editionen der Universitäts- und Landesbibliothek Darmstadt

# Inhaltsverzeichnis
[1) Allgemeine Bemerkungen](#1-allgemeine-bemerkungen)  
[2) Grundlegende Überlegungen](#2-grundlegende-%C3%BCberlegungen)  
[3) XML und XSLT](#3-xml-und-xslt)  
[4) XQuery](#4-xquery)  
[5) HTML](#5-html)  
[6) Javascript](#6-javascript)    
[7) Python](#7-python)  
[8) Anmerkungen](#8-anmerkungen)  


## 1) Allgemeine Bemerkungen
*“A team following a style guide helps everyone write code in a consistent way, and consistent code is easier to read and faster to update* (vgl. https://www.pullrequest.com/blog/create-a-programming-style-guide/)

## 2) Grundlegende Überlegungen

Grundsätzlich strebt das ZEiD größtenteils Standardnähe an, an entscheidenden Stellen weichen wir jedoch davon ab. Wir streben mindestens ein Beispiel für jede Abweichung an. Grundsätzlich gilt für offiziell und mehrfach genutzten Code im ZEiD **Lesbarkeit vor Kürze**. 
Verständlichkeit und Lesbarkeit sind wichtiger als die kürzestmögliche Schreibweise. 

### a) Großbuchstaben

Wenn Großbuchstaben genutzt werden, sollten sie stets eine tiefere Bedeutung haben. Für *Variablen* wird sprachenübergreifend **CamelCase** genutzt. Attribute, Klassen, und sonstige benannte Dinge werden in der Regel **klein** geschrieben.

### b) Einrückungen und Zeilenlängen

Es wird möglichst präzise eingerückt: Sofern in den verschiedenen Sprachen nicht genauer definiert wird mit 3 Leerzeichen eingerückt. 
Die Zeilenlänge soll ungefähr 120 Zeichen umfassen. Deutlich überlange Zeilen werden am letzen Attribut umgebrochen.

### c) Anführungszeichen

Wir verwenden einfache Anführungszeichen, so können in der HTML-Ausgabe Attribute korrekt mit doppelten Anführungszeichen ausgegeben werden. 

### d) Lücken im Stylebook

Sollte ein bestimmter Fall bisher nicht abgedeckt werden, besprechen wir den Umgang damit. Eine grundlegende Reihenfolge für den Umgang mit nicht definierten Fällen:  

&ensp;&ensp;&ensp;1) Orientierung an entsprechenden Standards       
&ensp;&ensp;&ensp;2) Orientierung an grundlegenden Überlegungen  
&ensp;&ensp;&ensp;3) Wo möglich und sinnvoll an **4) XQuery** orientieren  
&ensp;&ensp;&ensp;4) Dokumentieren und Diskussion mit dem Team suchen 


### 2.1) Namesgebung
*grundsätzlich werden sprechende Namen vergeben*.
#### a) Variablen
Variablen werden je nach Herkunft mit einem Präfix versehen. Projektvariablen werden nach dem Muster ```{projektkürzel}-{variablenName}``` benannt:
Variablen aus wdbplus werden mit dem Präfix ```wdb``` versehen. 
 *exemplarisch hier die CSS-Variablen aus EuReD*
```  :root {
    /*ZEiD-spezifische Farbe */
  --zeidMainColor: #54625e;
  /* projektspezifische Farben */
  --euredEntities:white;
  }
```

#### b) Klassen
Klassen werden je nach Herkunft mit einem Präfix versehen. Projektvariablen werden nach dem Muster ```{projektkürzel}-{KlassenName}``` benannt. *Beispiele einfügen*

#### c) IDs
IDs bekommen aus vermuteten negativen Auswirkungen keine Präfixe!



### 2.2) Kommentare
Grundsätzlich gilt: *So viele Kommentare wie möglich, so wenig wie möglich*   
&ensp;&ensp;&ensp;1) Kommentare stehen immer vor dem Code den sie beschreiben    
&ensp;&ensp;&ensp; 2) in den Sprachen wird genauer definiert  
&ensp;&ensp;&ensp; 3) Der Code soll bestmöglich beschrieben und dokumentiert werden

## 3) XML und XSLT
### 3.1) Generelles
Wo unter XSLT nicht genauer definiert gilt was in XML definiert wird. 
### 3.2) XML  
#### a) Einrückungen
um 3 Leerzeichen *Beispiel einfügen*  
#### b) Attribute und Elementnamen 
**camelCase**. So können sie von generischen Attributen abgesetzt werden. SCHREIEN wird vermieden.    
#### c) Kommentare
Kommentare werden mittels <xd:desc> umgesetzt: 
Beispiel: 
```<xd:doc>
    <xd:desc>
      <xd:p>Das folgende Template gibt ein <span class="lineNumber">$lineNumber</span> aus.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template name="lineNumber-span">
        $TemplateContent 
  </xsl:template>
```
### 3.3) XSLT
XSLT wird grundsätzlich wie XML behandelt.  
&ensp;&ensp;&ensp;- Alle Elemente werden umgebrochen; keine Verschachtelungen auf einer Zeile *Beispiel einfügen*  
&ensp;&ensp;&ensp;- Bei ```space="preserve"``` nur die gewünschten Umbrüche!   
&ensp;&ensp;&ensp;- leere Elemente werden self-closing mit Leerzeichen vor dem Slash  
&ensp;&ensp;&ensp;- externe Dokumente stets mit Variable zu Beginn des Dokuments einbinden  
&ensp;&ensp;&ensp;- jegliche Form von Text wird von <xsl:text> umschlossen    
&ensp;&ensp;&ensp;- Variablen werden sprechend im camelCase benannt

## 4) XQUERY
### 4.1) Kommentare
Jegliche Funktion wird mit xqDoc kommentiert *Beispiel einfügen*
### 4.2) Funktionen, Conditionals, und switch  
#### 4.2.1) Funktionen
Grundsätzlich wird unterschieden, ob etwas aufgerufen oder deklariert wird.
##### a) Deklarationen
Bei Funktionsdeklarationen werden die runden Klammern von Leerzeichen umgeben:  
&ensp;&ensp;&ensp;```declare function functionName ( $a as xs:string )```
##### b) Aufrufe
Funktionsaufrufe werden nicht mit Leerzeichen umschlossen, so grenzen sich Aufrufe von Deklarationen ab.

#### 4.2.2) Conditionals
"if ( … ) then (" in einer neuen Zeile um 2 Spaces einrücken, body von then auf neue Zeile, um 2 weitere Spaces einrücken; else hat gleiche Einrückung wie if, den body von else wie then um 2 Spaces eingerückt [Hintergrund: es ist klar, daß das if zur Variablendefinition gehört, die Einrückung der Inhalte von then und else sind gleich und else if führt nicht zu immer stärker eingerückten Blöcken.] 
Beispiel: 
```
    if ( $name = 'start' ) then ( 
         $someContent
        )
        else ()
```

#### 4.2.3) Switch
Grundsätzlich ebenfalls um 2 Spaces eingerückt; die einzelnen ```case``` ebenfalls um zwei Spaces. Die darunterliegenden ```return``` um weitere zwei. *Beispiel einfügen* 
### 4.3) Umgang mit Variablen
&ensp;&ensp;&ensp;-Vor aufeinander aufbauende ```let``` nur ein ```let```. Vor Beginn der Gruppe steht beschreibender Kommentar. Das zugehörige Komma steht eingerückt am Anfang der Zeile (leer leer komma leer)  
&ensp;&ensp;&ensp;-lange Variablendefitionen vor dem Operator umbrechen (+ oder ||)

### 4.4) Sequenzen
&ensp;&ensp;&ensp;-Sequenzen werden immer beim Definitionsbeginn der Klammern umgebrochen, der neue Teil der Sequenz wird eingerückt mit beginnendem Komma. Das erste ```if``` steht auf anderer Ebene.    
&ensp;&ensp;&ensp;-Alle Sequenz-Einträge sind auf einem Level eingerückt, d.h. erstes Element wird doppelt eingerückt.   
&ensp;&ensp;&ensp;- Eine Sequenz ist grundsätzlich ein Hinweis für ein neues ```let```  
 

### 4.5) Kurzformen
Einen Teil der gängigen Kurzschreibweisen vermeiden wir, einen Teil nicht. Für uns erschien Lesbarkeit und Verständlichkeit wichtiger als kürzestmöglicher Code.
&ensp;&ensp;&ensp;- *der Map-Operator* wird genutzt


## 5) HTML
HTML wird standardkonform angewandt:   
&ensp;&ensp;&ensp;- Klassennamen sind sprechend im camelCase zu vergeben  
&ensp;&ensp;&ensp;- Das ```<script>``` Tag darf nicht selbstschließend sein  
&ensp;&ensp;&ensp;- Auf custom-tags wird vollständig verzichtet  
&ensp;&ensp;&ensp;- aria-label und aria-role nutzen  
&ensp;&ensp;&ensp;- Langfristig: XHTML

## 6) Javascript
### 6.1) Variablendeklarationen
```let``` oder ```const``` eventuell vorkommende ```var``` müssen ersetzt werden. Für die Variablendeklaration gilt, analog zu XQuery, bei mehreren Deklarationen nur ein ```let```, das Komma ist umgebrochen.
### 6.2) Umbrüche
&ensp;&ensp;&ensp;- Analog zu XQuery  
&ensp;&ensp;&ensp;- lange Ketten mit . am Anfang umbrechen *Beispiel einfügen*  
&ensp;&ensp;&ensp;- Funktionsdeklarationen und Aufrufe wie in XQuery *Beispiel und Link einfügen*  
&ensp;&ensp;&ensp;- ternären Operator nach Möglichkeit vermeiden--> Lesbarkeit vor Kürze  
&ensp;&ensp;&ensp;- projektspezifisches Javascript sollte ein Objekt werden  *Beispiele einfügen* 



## 7) Python

Python ist die wohl am stärskten strukturierte Sprache. Hier machen ZEiD-Abweichungen wenig Sinn. **PEP8** (vgl. https://www.python.org/dev/peps/pep-0008/) wird exakt umgesetzt, mit der Ausnahme der Variablen. Diese werden, wie üblich, im **CamelCase** benannt.  

## 8) Anmerkungen
